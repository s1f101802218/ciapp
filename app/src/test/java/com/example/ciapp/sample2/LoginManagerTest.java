package com.example.ciapp.sample2;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LoginManagerTest {
    private LoginManager loginManager;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
        loginManager.register("Testuser1", "Password1234");
    }

    @Test
    public void testLoginSuccess() throws LoginFailedException {
        User user = loginManager.login("Testuser1", "Password1234");
        assertThat(user.getUsername(), is("Testuser1"));
        assertThat(user.getPassword(), is("Password1234"));
    }

    @Test(expected = LoginFailedException.class)
    public void testLoginWrongPassword() throws LoginFailedException {
        User user = loginManager.login("testuser1", "1234");
    }

    @Test(expected = LoginFailedException.class)
    public void testLoginUnregisteredUser() throws LoginFailedException {
        User user = loginManager.login("iniad", "Password1234");
    }

    @Test(expected = LoginFailedException.class)
    public void testCantPassPassword1() throws LoginFailedException {
        User user = loginManager.login("iniad", "assword1234");
    }

    @Test(expected = LoginFailedException.class)
    public void testCantPassPassword2() throws LoginFailedException {
        User user = loginManager.login("iniad", "Paasdfgaaassword");
    }

}